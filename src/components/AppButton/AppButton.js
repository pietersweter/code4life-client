import React from 'react'

import './AppButton.scss';

export default (props) => {
  return (
    <button 
      type="button"
      className={`AppButton AppButton-${props.buttonType}`}
    >
    { props.children }
    </button>
  )
}
