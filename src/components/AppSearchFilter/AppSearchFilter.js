import React, { Component } from 'react';


import './AppSearchFilter.scss';

import filters from '../../config/filters';
import questionsStatusToColor from '../../config/questionsStatusToColor';

class AppSeachFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMenu: false,
      filters: new Set(Object.values(filters))
    };

    this.toggleFilter = this.toggleFilter.bind(this);
    this.applyFilter = this.applyFilter.bind(this);
  }

  toggleFilter() {
    this.setState({
      showMenu: !this.state.showMenu
    }, () => {
      if (!this.state.showMenu) {
        this.setState({
          filters: new Set(Object.values(filters))
        }, () => {
          this.props.onChange(Array.from(this.state.filters));
        });
      }
    });
  }

  applyFilter(e) {
    const filterElement = e.target;
    console.log(filterElement.dataset.name);
    const filterName = filterElement.dataset.name;
    let newFilters = this.state.filters;

    if (this.state.filters.has(filterName)) {
      newFilters.delete(filterName);
      filterElement.classList.add('disabled');
    } else {
      newFilters.add(filterName);
      filterElement.classList.remove('disabled');
    }

    this.setState({
      filters: newFilters
    });
    this.props.onChange(Array.from(newFilters));
  }

	pickColor(status) {
		switch(status) {
			case filters.FINISHED:
				return 'FINISHED';
				break;
			case filters.IN_PROGRESS:
				return 'IN_PROGRESS';
				break;
			case filters.NOT_STARTED:
				return 'NOT_STARTED';
				break;
			default:
				break;
		}
	}

  render() {
    return (
      <div className="filter">
        <div className="toggle-filter" onClick={this.toggleFilter}>
          <span className="filter-icon" />
          <span className="filter-text">Filtruj Pytania</span>
        </div>

        {this.state.showMenu &&
          <div className="filter-menu">
            <div className="types">
              <span className="category-items">
              {
                Object.values(filters).map(questionsStatus => <div
                    key={questionsStatus}
                    className={this.pickColor(questionsStatus)}
                    data-name={questionsStatus}
                    onClick={this.applyFilter}
                  >
                    {questionsStatus}
                  </div>)
              }
              </span>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default AppSeachFilter;