import React, { Component } from 'react'
import './AboutUs.scss'

export default class AboutUs extends Component {
  constructor(props){
    super(props);
    this.state = { 

    }
  }

  render () { 
    return(
      <div className="AboutUs">
        <div className="AboutUs__columns">
        <div className="AboutUs__column">
          <video
            className="AboutUs__video"
            autoPlay="autoplay"
            type="video/mp4"
            src="/about-us.mp4"
            width="600"
            height="500">
            Sorry, your browser doesn't support embedded videos.
          </video>  
        </div>
          <div className="AboutUs__column">
          <div className="AboutUs__text">
            <p> Fundacja JiM jest największą w Polsce fundacją, niosącą pomoc dzieciom 
            z autyzmem. Fundacja walczy o lepszy świat dla nich, wpływając na rozwiązania prawne.  </p>
            <p> Opiekuje się bezpośrednio ponad 7 000 dzieci z całej Polski, zapewniając i finansując niezbędną terapię oraz wspierając rodziców poprzez wiedzę szkolenia i pomoc prawną.
            Co roku, lekarze z Kliniki JiM diagnozują bezpłatnie kilkaset maluchów. </p> 
            Ponadto Fundacja prowadzi przedszkole i szkoły dla dzieci z autyzmem oraz wspiera osoby dorosłe. Tworzy kampanie społeczne niosące wiedzę.
          </div>
          </div>
      </div>
      </div>
    )
  }
}
