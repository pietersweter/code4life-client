import React from 'react';
import './AppCircleSpinner.sass';

const AppCircleSpinner = () => {
  return (
    <div className="AppCircleSpinner spin-a spin-b App__animate-fade">
      <div className="inner-circle" />
    </div>
  );
};

export default AppCircleSpinner;