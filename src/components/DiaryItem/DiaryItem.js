import React, { Component } from 'react'
import './DiaryItem.scss'

export default class DiaryItem extends Component  {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  handleClick = (e) => {
    e.preventDefault();
    this.props.handleClick(this, e);
  }

  render () {
    return (
      <div className="DiaryItem" onClick={this.handleClick}>

      </div>
    )
  }
}
