import React, { Component } from 'react';

import './AppSearchBox.scss';

class AppSearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.updateTimeout = null;

    this.UPDATE_TIMER = 500;
  }

  handleChange(e) {
    const query = e.target.value;
    clearTimeout(this.updateTimeout);
    this.setState({ value: query }, () => {
      this.updateTimeout = setTimeout(()=>{
        this.props.onChange(this.state.value);
      }, this.UPDATE_TIMER);
    });
  }

  handleClear(e) {
    e.preventDefault();
    clearTimeout(this.updateTimeout);
    this.setState({
      value: ''
    }, () => {
      this.updateTimeout = setTimeout(()=>{
        this.props.onChange(this.state.value);
      }, this.UPDATE_TIMER);
    });
  }

  render() {
    return (
      <form className="AppSearchBox" onSubmit={(e) => e.preventDefault()}>
        <div className="AppSearchBox__wrapper">
          <input className="AppSearchBox__input" type="text" placeholder="Szukaj obserwacji" value={this.state.value} onChange={this.handleChange}/>
          <div className="AppSearchBox__icon" />
          <a href="#" className={`AppSearchBox__clear ${this.state.value !== '' ? "AppSearchBox__visible" : ""}`}  onClick={this.handleClear} />
        </div>
      </form>
    );
  }
}

export default AppSearchInput;