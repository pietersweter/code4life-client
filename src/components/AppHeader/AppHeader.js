import React, { Component } from 'react'

import './AppHeader.scss';

export default class AppHeader extends Component {
  constructor(props) {
    super(props);

    const navigationLinks = [
      {
        linkType: 'text',
        linkText: 'O nas',
        linkRedirection: 'about',
        availableAt: ['notLoggedIn', 'parent']
      },
      {
        linkType: 'text',
        linkText: 'Strona główna',
        linkRedirection: 'home',
        availableAt: ['notLoggedIn', 'parent']
      },
      {
        linkType: 'text',
        linkText: 'Szkoły specjalistyczne',
        linkRedirection: 'school',
        availableAt: ['parent']
      },
      {
        linkType: 'text',
        linkText: 'Specjaliście',
        linkRedirection: 'doctor',
        availableAt: ['parent']
      },
      {
        linkType: 'button',
        linkText: 'Zaloguj',
        linkRedirection: 'login',
        availableAt: ['notLoggedIn']
      },
      {
        linkType: 'button',
        linkText: 'Zarejestruj',
        linkRedirection: 'register',
        availableAt: ['notLoggedIn']
      },
      {
        linkType: 'button',
        linkText: 'Wyloguj',
        linkRedirection: 'logout',
        availableAt: ['parent']
      }
    ];

    // switch(this.props.currentUser) {
    //   case 'notLoggedIn':
    //     navigationLinks.push(
    //       {
    //         linkType: 'button',
    //         linkText: 'Zaloguj',
    //         linkRedirection: 'login'
    //       },
    //       {
    //         linkType: 'button',
    //         linkText: 'Zarejestruj',
    //         linkRedirection: 'register'
    //       }
    //     );
    //     break;
    //   case 'parent':
    //     navigationLinks.push(
    //       {
    //         linkType: 'text',
    //         linkText: 'Szkoły specjalistyczne',
    //         linkRedirection: 'school'
    //       },
    //       {
    //         linkType: 'button',
    //         linkText: 'Wyloguj',
    //         linkRedirection: 'logout'
    //       }
    //     );
    //     break;
    //   default:
    //     break;
    // }
    
    this.state = {
      navigationLinks: navigationLinks
    };

    // this.updateHeader = this.updateHeader.bind(this);
  }

  // updateHeader() {
  //   const newLinks = [
  //     {
  //       linkType: 'text',
  //       linkText: 'O nas',
  //       linkRedirection: 'about'
  //     },
  //     {
  //       linkType: 'text',
  //       linkText: 'Strona główna',
  //       linkRedirection: 'home'
  //     }
  //   ];
  //   console.log('~~~~~~~~~~~~~~~');
  //   console.log(this.props.currentUser);
  //   console.log('~~~~~~~~~~~~~~~');
  //   switch(this.props.currentUser) {
  //     case 'notLoggedIn':
  //       newLinks.push(
  //         {
  //           linkType: 'button',
  //           linkText: 'Zaloguj',
  //           linkRedirection: 'login'
  //         },
  //         {
  //           linkType: 'button',
  //           linkText: 'Zarejestruj',
  //           linkRedirection: 'register'
  //         }
  //       );
  //       break;
  //     case 'parent':
  //       newLinks.push(
  //         {
  //           linkType: 'text',
  //           linkText: 'Szkoły specjalistyczne',
  //           linkRedirection: 'school'
  //         },
  //         {
  //           linkType: 'button',
  //           linkText: 'Wyloguj',
  //           linkRedirection: 'logout'
  //         }
  //       );
  //       break;
  //     default:
  //       break;

  //     this.setState({
  //       navigationLinks: newLinks
  //     });
  //   }
  // }

  handleClick(selectedLinkRedirection) {
    this.props.selectView(selectedLinkRedirection);
  }

  render() {
    // this.updateHeader();
    return (
    <header className="AppHeader">
      <div className="AppHeader__logo"></div>
      <nav className="AppHeader__nav">
        { this.state.navigationLinks &&
          this.state.navigationLinks
            .filter(navigationLink => navigationLink.availableAt.includes(this.props.currentUser))
            .map((navigationLink, linkIndex) => (
            <div
              key={navigationLink.linkText}
              onClick={()=>this.handleClick(navigationLink.linkRedirection)} 
              className={`${this.props.currentStatus === navigationLink.linkRedirection ? 'active' : '' } AppHeader__link ${navigationLink.linkType === 'button' ? 'AppHeader__link-button' : 'AppHeader__link-text'}`}>
                {navigationLink.linkText}
            </div>
          ))
        }
      </nav>
    </header>
    )
  }
}
