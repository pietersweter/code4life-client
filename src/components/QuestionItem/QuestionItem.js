import React, { Component } from 'react';
import './QuestionItem.scss';

import filters from '../../config/filters';
import questionsStatusToColor from '../../config/questionsStatusToColor';

export default class QuestionItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}

		// this.handleClick = this.handleClick.bind(this);
	}

	// handleClick(selectedQuestion) {
	// 	this.props.handleClick(selectedQuestion);
	// }

	_mock_getRandomStatus() {
		const tmpRand = Math.random();
		const possibleStatuses = ['not started', 'in progress', 'finished'];
		let randomStatus = possibleStatuses[0];
		const interval = 1/possibleStatuses.length;
		for (let i=0; i<=possibleStatuses.length; i++) {
			if (tmpRand >= i*interval) {
				randomStatus = possibleStatuses[i];
			}
		}
		return randomStatus;
	}

	pickColor(status) {
		switch(status) {
			case filters.FINISHED:
				return 'FINISHED';
				break;
			case filters.IN_PROGRESS:
				return 'IN_PROGRESS';
				break;
			case filters.NOT_STARTED:
				return 'NOT_STARTED';
				break;
			default:
				break;
		}
	}

	render() {
		// console.log(this.props.question);
		return (
			<div className="QuestionItem" onClick={()=>{this.props.handleClick(this.props.firstQuestionId, this.props.id)}} href="#">
				<div className={`QuestionItem__circle QuestionItem__circle-${this.pickColor(this.props.question.status)}`}>
					<img src={`${this.props.question.imageUrl}`} alt="Question" className="QuestionItem__image"/>
				</div>

				<div className="QuestionItem__body">
					{ this.props.question.description}
				</div>
				<div className="QuestionItem__status">
					{ this.props.question.status ? this.props.question.status : this._mock_getRandomStatus()}
				</div>
			</div>
		)
	}
}