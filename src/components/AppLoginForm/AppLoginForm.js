import React, { Component } from 'react';
import './AppLoginForm.scss';
import axios from 'axios';

export default class AppLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {
        email: '',
        password: ''
      }
    };
    this.submit = this.submit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  submit(e) {
    e.preventDefault();
    if (this.state.userData.email && this.state.userData.password) {
      console.log({
        email: this.state.userData.email,
        password: this.state.userData.password
      })
      axios.post('https://autismtest.azurewebsites.net/api/Account/Login', {
        email: this.state.userData.email,
        password: this.state.userData.password
      }).then(resp => resp.data)
        .then (data => {
        
        localStorage.setItem('email', data.email);
        localStorage.setItem('testId', data.testId);
        this.props.fillQuestions(data.exercises);

        if (this.state.userData.email==='admin@admin.com')  {     // This needs to be refactored tho ;)
          console.log(data);
          this.props.fillEmails(data);
          this.props.selectView('home');
          this.props.selectUser('doctor');
        } else {
          this.props.selectView('home');
          this.props.selectUser('parent');
        }
      }).catch(err => {

      })
    } else {
      alert('Proszę uzupełnić wszystkie pola')
    }
  }

  handleInputChange(event) {
    const newUserData = this.state.userData;
    newUserData[event.target.name] = event.target.value;
    this.setState({
      userData: newUserData
    });
  }

  render() {
    return (
      <div className="AppLoginForm">
        <div className="AppLoginForm__columns">
          <div className="AppLoginForm__column">
            <div className="AppLoginForm__image"/>
          </div>
          <div className="AppLoginForm__column">
            <form className="AppLoginForm__content">
            <div className="AppLoginForm__text">Email: </div>
            <input className="AppLoginForm__input" type="text" name="email" value={this.state.userData.email} onChange={this.handleInputChange}/>
            <div className="AppLoginForm__text">Hasło: </div>
            <input className="AppLoginForm__input" type="password" name="password" value={this.state.userData.password} onChange={this.handleInputChange}/>
            <button className="AppLoginForm__button" onClick={this.submit}>Login</button>
          </form>
          </div>
        </div>
      </div>
      
    )
  }
}
