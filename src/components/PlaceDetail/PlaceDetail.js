import React, { Component } from 'react';
import './PlaceDetail.scss';

export default class PlaceDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
      icon: ''
    }
    
    if(props.type === 'doctor') {
      this.state.icon = "https://image.flaticon.com/icons/svg/921/921130.svg"
    } else {
      this.state.icon = "https://image.flaticon.com/icons/svg/1138/1138050.svg"
    }
	}

	handleClick = (e) => {
		e.preventDefault();
		this.props.handleClick(this, e);
  }
  

	render() {
		return (
      <div className="PlaceDetail">
        <div className="PlaceDetail__container">
          <img className="PlaceDetail__image" src = {`${this.state.icon}`} alt="Icon"/>
          <div className="PlaceDetail__item"> {this.props.place.name} </div>
          <div className="PlaceDetail__item"> {this.props.place.address} </div>
        </div>
      </div>
    )
	}
}